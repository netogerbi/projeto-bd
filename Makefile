
infra: stop mariadb neo4j redis

mariadb:
	docker run --rm --detach --name mariadb \
		-p 3306:3306 \
		-v $(CURDIR)/db/data:/var/lib/mysql \
		-e MYSQL_DATABASE=marketplace \
		-e MYSQL_USER=root \
		-e MYSQL_ALLOW_EMPTY_PASSWORD=yes \
		mariadb:10.4

	@echo "[mariadb] waiting mariadb to start up..."
	sleep 5

neo4j:
	docker run --rm  --detach --name neo4j \
        -p 7687:7687 \
        -p 7474:7474 \
        -e NEO4J_AUTH=neo4j/secret \
        --user=$(shell id -u):$(shell id -g) \
        neo4j:3.5.3

	@echo "[neo4j] waiting neo4j to start up..."
	sleep 5

redis:
	docker run --rm  --detach --name redis \
        -p 6379:6379 \
        redis:3.2.11

stop:
	-docker rm -f mariadb
	-docker rm -f neo4j
	-docker rm -f redis