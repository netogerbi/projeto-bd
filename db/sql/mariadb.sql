-- drop all
drop table if exists fornecedor;
drop table if exists produto;
drop table if exists cliente;
drop table if exists pedido;
drop table if exists intems_pedido;
drop sequence if exists marketplace_seq;

create sequence if not exists marketplace_seq;

create table if not exists fornecedor (
	id bigint default (next value for marketplace_seq) primary key,
	nome varchar(100),
	cnpj varchar(14)
)

create table if not exists produto (
	id bigint default (next value for marketplace_seq) primary key,
	fornecedor_id bigint references fornecedor(id),
	nome varchar(100),
	descricao varchar(255),
	preco bigint,
	codigo varchar(30) unique
);

create table if not exists cliente (
	id bigint default (next value for marketplace_seq) primary key,
	nome varchar(100),
	cpf varchar(11),
	endereco varchar(255),
	telefone varchar(15)
);

create table if not exists pedido (
	id bigint default (next value for marketplace_seq) primary key,
	cliente_id bigint references cliente(id),
	valor bigint,
	data datetime
);

create table if not exists intems_pedido (
	id_pedido bigint references pedido(id),
	id_produto bigint references produto(id)
);


insert into fornecedor (nome, cnpj) values ('Gerbi LTDA', '29948804000107');
insert into fornecedor (nome, cnpj) values ('Embacadero', '51434040000182');

insert into produto (nome, fornecedor_id, descricao, preco, codigo)
values ('Galaxy S10', 1, 'Best smartphone on the market', 3500, 'AX12D34');

insert into produto (nome, fornecedor_id, descricao, preco, codigo)
values ('Rat server', 2, 'application server for windows', 10500, 'DX34GAG');

insert into cliente (nome, cpf, endereco, telefone)
values ('Fabio Rubim', '17146933091', 'Rua do acido', '11329238747')


insert into pedido (cliente_id, valor, `data`)
values (5, 10500, now());

insert into intems_pedido (id_pedido, id_produto) values (5, 4);

select * from marketplace.produto;
select * from marketplace.fornecedor;
select * from marketplace.cliente;
select * from marketplace.pedido;
select * from marketplace.intems_pedido;