package repository;

import domain.Produto;
import utils.MariaDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

public class ProdutoDAO {

    private MariaDB mdb = new MariaDB();
    private Produto produto;


    public ProdutoDAO() {
    }


    public Produto getProduto() {
        return produto;
    }


    public void setProduto(Produto obj) {
        this.produto = obj;
    }

    public Produto create(Produto obj) {
        String query = "INSERT INTO produto (id, nome, preco) "
                + "VALUES (" + obj.getId() + "," + obj.getNome() + "," + obj.getNome() + ")";
        this.mdb.setQuery(query);
        this.mdb.execute();
        ResultSet resultSet = this.mdb.execute();
        return rowMapper(resultSet);
    }

    public Produto find(int id) {
        String query = format("SELECT id, nome, preco FROM produto WHERE id = %d", id);
        this.mdb.setQuery(query);
        this.mdb.execute();
        ResultSet resultSet = this.mdb.execute();
        return rowMapper(resultSet);
    }

    public List<Produto> findAll() {
        String query = "SELECT id, nome, preco FROM produto";
        this.mdb.setQuery(query);
        this.mdb.execute();
        ResultSet resultSet = this.mdb.execute();
        return Stream.of(resultSet)
                .map(this::rowMapper)
                .collect(Collectors.toList());
    }


    public Produto update(Produto obj) {
        String query = "INSERT INTO produto (id, nome, preco) "
                + "VALUES (" + obj.getId() + "," + obj.getNome() + "," + obj.getNome() + ")";
        this.mdb.setQuery(query);
        ResultSet resultSet = this.mdb.execute();
        return rowMapper(resultSet);
    }

    public void delete(Produto obj) {
        String query = "DELETE FROM produto WHERE id = '" + obj.getId() + "'";
        this.mdb.setQuery(query);
        this.mdb.execute();
    }

    private Produto rowMapper(ResultSet resultSet) {
        Produto produto = null;
        try {
            resultSet.next();
            int id = resultSet.getInt("id");
            String nome = resultSet.getString("nome");
            float preco = resultSet.getFloat("preco");
            produto = new Produto(id, nome, preco);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return produto;
    }
}
