package main;

//import org.neo4j.driver.Record;
import domain.Produto;
import repository.ProdutoDAO;
import utils.MariaDB;
import utils.Neo4j;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Marketplace {

    public static void main(String[] args) {
        MariaDB mdb = new MariaDB();
        mdb.setQuery("SHOW TABLES");
        ResultSet rs = mdb.execute();
        System.out.println(rs);

        ProdutoDAO dao = new ProdutoDAO();
        Produto produto = dao.find(3);
        System.out.println(produto);

        List<Produto> produtos = dao.findAll();
        System.out.println(produtos);


//        Neo4j cypher = new Neo4j();
//        Record r = cypher.queryForObject("MATCH (n) RETURN n LIMIT 1", new HashMap<>(), record -> {
//            return record;
//        });
        //System.out.println(r);

//        List<Record> records = cypher.queryForList("MATCH (n) RETURN n LIMIT 10", new HashMap<>(), record -> {
//            return record;
//        });
//        System.out.println(records);

//        String query = "CREATE (User1: Pessoa {nome: {name}, descricao: {description}, numero: {number}})";
//
//        Map<String, Object> params = new HashMap<>();
//        params.put("name", "Brenno");
//        params.put("description", "blah blah");
//        params.put("number", "10");
//        cypher.update(query, params);
//
//        cypher.close();
    }

}
