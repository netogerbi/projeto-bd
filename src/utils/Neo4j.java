package utils;


import org.neo4j.driver.*;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Neo4j implements AutoCloseable {

    //  Database credentials
    static final String DB_URI = "bolt://localhost:7687";
    static final String USER = "neo4j";
    static final String PASS = "secret";

    private final Driver driver;

    public Neo4j() {
        driver = GraphDatabase.driver(DB_URI, AuthTokens.basic(USER, PASS));
    }

    @Override
    public void close() {
        try {
            driver.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <R> R queryForObject(String query, Map<String, Object> params, Function<Record, R> recordMapper) {
        try (Session session = driver.session()) {
            StatementResult result = session.run(query, params);
            Record record = result.single();
            return recordMapper.apply(record);
        }
    }

    public <R> List<R> queryForList(String query, Map<String, Object> params, Function<Record, R> recordMapper) {
        try (Session session = driver.session()) {
            StatementResult result = session.run(query, params);
            List<Record> records = result.list();
            return records.stream()
                    .map(recordMapper)
                    .collect(Collectors.toList());
        }
    }

    public void update(String query, Map<String, Object> params) {
        try (Session session = driver.session()) {
            session.writeTransaction(tx -> tx.run(query, params));
        }
    }

}
