package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MariaDB {
	
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
    static final String DB_NAME = "marketplace";
    static final String DB_URL = "jdbc:mariadb://localhost:3306/" + DB_NAME;

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "";
    
    private String query;
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    
    
    
    public MariaDB() {
	}

	private void connect() {
    	
    	try {
    		
			Class.forName(JDBC_DRIVER);
			System.out.println("Connecting to a selected database...");
	        this.conn = DriverManager.getConnection(DB_URL, USER, PASS);
	        System.out.println("Connected database successfully...");
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
        
    }
    
    public ResultSet execute() {
    	
    	this.connect();
    	
		try {
			
			System.out.println("Executing query statement...");
			this.stmt = conn.createStatement();
			stmt.executeUpdate(this.query);
			this.rs = stmt.getResultSet();
			this.closeConection();
			return this.rs;

		} catch (SQLException se) {
			this.closeConection();
			se.printStackTrace();
			return null;
		}
    	
    }
    
    private void closeConection() {
		if (this.stmt != null || this.conn != null) {
	        try {
				this.conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	    }
    }
	

    public String getQuery() {
		return query;
	}



	public void setQuery(String query) {
		this.query = query;
	}
}
